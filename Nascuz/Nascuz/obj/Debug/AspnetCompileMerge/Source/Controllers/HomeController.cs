﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nascuz.Models;

namespace Nascuz.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.SuccessMsg = ".";

            return View();
        }
        [HttpPost]
       [ValidateAntiForgeryToken]
        public ActionResult Contact([Bind(Include = "Name,Email,Phone,Message")] Contact contact)
        {
            ViewBag.Message = "Your contact page.";
            if (ModelState.IsValid)
            {
                //db.Contacts.Add(contact);


                //now do the HTML formatting

                //now append it to the body of the mail

                //  await db.SaveChangesAsync();
                ViewBag.Message = "Here we are";
                ViewBag.SuccessMsg = "Thank you for contacting us will get in touch very soon";
                ModelState.Clear();
                return View();
            }

            ViewBag.SuccessMsg = "Please fill missing fields on your form";
            
            return View(contact);
        }

        public ActionResult missionandvision()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult coreandgoverning()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult nazmandate()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult transformation()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult strategicvision()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult strategicmission()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult hyperinflation()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult nazcuzrevival()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult currentchallenges()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult  addresschallenges()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult conclusion()
        {

            return View();
        }

        public ActionResult boardmembers()
        {

            return View();
        }
        public ActionResult technicalboard()
        {

            return View();
        }


        public ActionResult secremanage()
        {

            return View();
        }



    }
}